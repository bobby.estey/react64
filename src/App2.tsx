import React, { useState, useEffect } from "react";
import useFetch from "./components/hooks/useFetch";
import "./App.css";

import BasicFetch from "./components/fetchAPI/BasicFetch";
import DialogExample from "./components/ui/dialog/DialogExample";
import FetchSpring from "./components/fetchAPI/FetchSpring";
import { Parent } from "./components/callbacks/Parent";
import Series from "./components/series/Series";
import UseStateCustomHook from "./components/fetchAPI/UseStateCustomHook";

const title = "react64 - React Demonstration Site";

// const [data, setData] = useState<Array<Object>>([]);

function App() {

	// const { data, loading, error} = useFetch("https://v2.jokeapi.dev/joke/Any");

	// if (loading) return <h1> LOADING...</h1>;

	// if (error) console.log(error);

	// console.log(data?['setup']);

	return (
		<div className="App">
			{title}
			<br></br>
			<br></br>
			<BasicFetch />
		</div>
	);
}

export default App;
