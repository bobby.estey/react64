import { useEffect, useState } from "react";

interface IchildProps {
  name: string;
  updateName: (arg: string) => void;
}
export const Child: React.FC<IchildProps> = ({ name, updateName }) => {
  const [childName, setChildName] = useState<string>("");

  useEffect(() => {
    setChildName(name);
  }, [name]);

  return (
    <section>
      <h2> {childName} </h2>
      <button onClick={() => updateName("Child")}>child</button>
    </section>
  );
};
