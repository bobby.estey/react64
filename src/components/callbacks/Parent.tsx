// https://javascript.plainenglish.io/passing-data-from-child-to-parent-component-in-typescript-react-92ab6d03ceb1
import { useState } from "react";

import { Child } from "./Child";

export const Parent: React.FC = () => {
  const [parentName, setParentName] = useState<string>("Parent");

  const updateName = (name: string): void => {
    setParentName(name);
  };
  return (
    <div>
      {/* <FirstChild name={parentName} updateName={updateName} /> */}
      <Child name={parentName} updateName={updateName} />
    </div>
  );
};
