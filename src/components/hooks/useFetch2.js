// https://www.youtube.com/watch?v=ADe72qZ1_jc
import {useState, useEffect} from 'react';

function useFetch2(url, options) {

    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [data, setData] = useState([])

    // useEffect(() => {
        const fetchData = async () => {
    
            try {
                const response = await fetch(url, options)

                const data = await response.json()

                setData(data)
                setLoading(false)
            } catch (error) {
                setError(error)
                setLoading(false)
            }
        }
    
        fetchData()

    // })

    return { data, loading, error }
}

export default useFetch2;
