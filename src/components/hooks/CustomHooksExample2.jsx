import React from "react";
import useFetch2 from "./useFetch2";

function CustomHooksExample2() {
  const url = "https://jsonplaceholder.typicode.com/users";

  // show in console
  const response = useFetch2(url, {});
  console.log("console: " + response);

  // destructure
  const { data, loading, error } = useFetch2(url, {});

  if (loading) return <h3>Loading...</h3>;

  return (
    <div>
      {data.map((field) => (
        <h3 key={field.id}>
          {field.name} : {field.id} : {field.email}{" "}
        </h3>
        // <h3 key={field.score}>{field.score} </h3>
        // <h3 key={field.id}>{field.name} : {field.lastname} </h3>
      ))}
    </div>
  );
}

export default CustomHooksExample2;
