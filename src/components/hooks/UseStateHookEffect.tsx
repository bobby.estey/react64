// https://reactjs.org/docs/hooks-state.html

import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";

const UseStateHookEffect = () => {
  // count - new state variable
  // setCount - function to update the variable
  // useState hook the creates the state, function and initializes the state variable
  // brackets are known as "array destructuring" - useState returns an array size of 2 elements
  // count = array[0] and setCount = array[1]
  const [count, setCount] = useState(0);

  function returnHTML() {
    return <div>this should work</div>;
  }

  useEffect(() => {
    document.title = `Effect clicked ${count} times`;
    returnHTML();
  });

  return (
    <div>
      <p>DOM clicked {count} times</p>
      <button id="buttonClick" onClick={() => setCount(count + 1)}>
        Click me
      </button>
    </div>
  );
};

ReactDOM.render(<UseStateHookEffect />, document.getElementById("root"));

export default UseStateHookEffect;
