export const DialogCardSX = {
  fontFamily: "Lato",
  style: "Medium",
  fontSize: "18px",
  height: "456px",
  // width: "613px",
  color: "#000000",
  backgroundColor: "#ffffff",
};

export const DialogTitleSX = {
  fontFamily: "Lato",
  style: "Medium",
  fontSize: "18px",
  height: "40px",
  color: "#ffffff",
  backgroundColor: "#254267",
};

export const DialogHeaderBandSX = {
  fontFamily: "Lato",
  style: "Regular",
  fontSize: "16px",
  color: "#ed1c24",
  backgroundColor: "#ebebeb",
  fontWeight: "bold",
};

export const DialogFooterBandSX = {
  fontFamily: "Lato",
  style: "Regular",
  fontSize: "16px",
  color: "#000000",
  backgroundColor: "#ebebeb",
  fontWeight: "bold",
};

export const DialogPanelSX = {
  display: "flex",
  flexDirection: "column",
  justifyContent: "flex-start",
  p: 1,
  m: 1,
  bgcolor: "#fffff",
  borderRadius: 1,
};

export const DialogLabelSX = {
  fontFamily: "Lato",
  style: "Black",
  fontSize: "16px",
  color: "#00529a",
  backgroundColor: "#ffffff",
  fontWeight: "bold",
};

export const DialogSelectSX = {
  my: 1,
  width: "100%",
};

export const DialogTextFieldSX = {
  my: 1,
  width: "100%",
};

export const DialogButtonAddSX = {
  fontFamily: "Lato",
  style: "Regular",
  fontSize: "16px",
  color: "#ffffff",
  backgroundColor: "#00529a",
  mx: 10,
  px: 10,
};

export const DialogButtonCancelSX = {
  fontFamily: "Lato",
  style: "Regular",
  fontSize: "16px",
  color: "#ffffff",
  backgroundColor: "#000000",
  mx: 10,
  px: 10,
};
