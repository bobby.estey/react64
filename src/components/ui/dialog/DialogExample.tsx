import React, { useState } from "react";

import {
  Box,
  Button,
  Dialog,
  DialogTitle,
  FormControl,
  IconButton,
  MenuItem,
  TextField,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import Select, { SelectChangeEvent } from "@mui/material/Select";

import {
  DialogCardSX,
  DialogButtonAddSX,
  DialogButtonCancelSX,
  DialogFooterBandSX,
  DialogHeaderBandSX,
  DialogLabelSX,
  DialogPanelSX,
  DialogSelectSX,
  DialogTextFieldSX,
  DialogTitleSX,
} from "./DialogSX";

const DialogExample = () => {
  const emailRegex = /\S+@\S+\.\S+/;  // email RegEx

  const [dialogOpen, setDialogOpen] = useState(false);
  const [title, setTitle] = useState("Add Email");
  const [dialogBand, setDialogBand] = useState("** DIALOG BAND **");
  const [menuValue, setMenuValue] = useState("100");
  const [emailValue, setEmailValue] = useState("");
  const [buttonAddDisabled, setButtonAddDisabled] = useState(true);

  const validateForm = (event: any) => {
    setEmailValue(event.target.value as string);
    if (emailRegex.test(emailValue) && menuValue !== "") {
      setButtonAddDisabled(false);
    } else {
      setButtonAddDisabled(true);
    }
  };

  const handleDialogOpen = () => {
    setDialogOpen(true);
  };

  const handleDialogClose = () => {
    setDialogOpen(false);
    setMenuValue("100");
    setEmailValue("");
    window.location.reload();
  };

  const handleSelectChange = (event: SelectChangeEvent) => {
    setMenuValue(event.target.value as string);
    if (emailRegex.test(emailValue) && menuValue !== "") {
      setButtonAddDisabled(false);
    } else {
      setButtonAddDisabled(true);
    }
  };

  return (
    <div>
      <Button variant="outlined" onClick={handleDialogOpen}>
        Open Dialog
      </Button>
      <Dialog
        sx={DialogCardSX}
        open={dialogOpen}
        onClose={handleDialogClose}
        fullWidth={true}
      >
        <DialogTitle sx={DialogTitleSX}>
          <Box display="flex" alignItems="center" justifyContent={"center"}>
            {title}
            <Box
              display="flex"
              alignItems="flex-end"
              justifyContent={"flex-end"}
            >
              <IconButton id="IconButton" onClick={handleDialogClose}>
                <CloseIcon sx={DialogTitleSX} />
              </IconButton>
            </Box>
          </Box>
        </DialogTitle>

        <Box
          sx={DialogHeaderBandSX}
          display="flex"
          alignItems="center"
          justifyContent={"center"}
        >
          {dialogBand}
        </Box>

        <Box sx={DialogPanelSX}>
          <Box sx={DialogLabelSX}>Name</Box>
          <Box>
            <FormControl fullWidth>
              <Select
                sx={DialogSelectSX}
                id="name-selection"
                value={menuValue}
                onChange={handleSelectChange}
              >
                <MenuItem value={100}>
                  BOBBY.ESTEY
                </MenuItem>
                <MenuItem value={200}>
                  ESTER.ESTEY
                </MenuItem>
                <MenuItem value={300}>
                  REBBY.ESTEY
                </MenuItem>
              </Select>
            </FormControl>
          </Box>
          <Box sx={DialogLabelSX}>Email</Box>
          <Box>
            <TextField
              sx={DialogTextFieldSX}
              id="emailTextField"
              variant="outlined"
              onChange={validateForm}
            />
          </Box>
          <Box display="flex" alignItems="center" justifyContent={"center"}>
            <Button
              sx={DialogButtonAddSX}
              variant="contained"
              disabled={buttonAddDisabled}
              onClick={() => setDialogOpen(false)}
            >
              Add
            </Button>
            <Button
              sx={DialogButtonCancelSX}
              variant="contained"
              onClick={handleDialogClose}
            >
              Cancel
            </Button>
          </Box>
        </Box>

        <Box
          sx={DialogFooterBandSX}
          display="flex"
          alignItems="center"
          justifyContent={"center"}
        >
          {dialogBand}
        </Box>
      </Dialog>
    </div>
  );
};

// ReactDOM.render(<DialogExample />, document.getElementById("root"));

export default DialogExample;
