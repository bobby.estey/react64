export type DataGridDetailedRecord = {
  userFunction: string;
  email: string;
};

export type DataGridProperties = {
  dataGridDetailedRecords: DataGridDetailedRecord[];
};
