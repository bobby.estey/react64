import React, { useEffect, useState, useRef } from "react";
import { AgGridReact } from "ag-grid-react";
import "./ManageEmail.css";
import { DataGridProperties } from "./DataGridTypes";

import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-alpine.css";
import { GridReadyEvent } from "ag-grid-community";

const ManageEmailGridComponent: React.FC<DataGridProperties> = (props) => {
  const { dataGridDetailedRecords } = props;

  const [rowData, setRowData] = useState([]);
  const gridRef = useRef(null);

  const [data, setData] = useState([]);

  const [columnDefs] = useState([
    {
      field: "FUNCTION",
      sortable: true,
      filter: true,
      resizable: true,
      width: 400,
    },
    {
      field: "EMAIL",
      sortable: true,
      filter: true,
      resizable: true,
      width: 583,
    },
  ]);

  const gridOptions = {
    // PROPERTIES
    // Objects like myRowData and myColDefs would be created in your application
    // rowData: myRowData,
    columnDefs: columnDefs,
    // pagination: true,
    // rowSelection: "single",

    // EVENTS
    // Add event handlers
    // onRowClicked: () => console.log("A row was clicked"),
    // onColumnResized: () => console.log("A column was resized"),
    // onGridReady: () => console.log("The grid is now ready"),

    // CALLBACKS
    // getRowHeight: (params) => 25

    // menuTabs: [],
    // suppressMenu: true,
    // suppressMenuHide: false,
    // groupHideOpenParents: true,
  };

  const getData = () => {
    fetch("data.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then(function (response) {
        console.log(response);
        return response.json();
      })
      .then(function (myJson) {
        console.log(myJson);
      });
  };

  useEffect(() => {
    fetch("data.json", {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    })
      .then((result) => result.json())
      .then((rowData) => setRowData(rowData));
  }, []);

  const onButtonClick = (e: any) => {
    // @ts-ignore
    const selectedNodes = gridRef.current.api.getSelectedNodes();
    const selectedData = selectedNodes.map((node: { data: any }) => node.data);
    const selectedDataStringPresentation = selectedData
      .map(
        (node: { FUNCTION: any; EMAIL: any }) =>
          `${node.FUNCTION} ${node.EMAIL}`
      )
      .join(", ");
    alert(`Selected nodes: ${selectedDataStringPresentation}`);
  };

  // const onGridReady = (params: GridReadyEvent) => {
  //   params.api.addGlobalListener((type: string, e: any) => {
  //     if (type === "rowClicked" || type === "rowSelected") {
  //       console.log(type, "from global event handler");
  //     }
  //   });
  // };

  return (
    <div id="manageEmailTable" className="manageEmailTable">
      {/* {JSON.stringify(rowData)} */}
      <AgGridReact
        className="ag-theme-alpine"
        gridOptions={gridOptions}
        ref={gridRef}
        rowData={rowData}
        columnDefs={columnDefs}
        rowSelection="multiple"
        // onRowSelected={(e) => console.log("row(s) selected", e.rowIndex)}
        // onRowClicked={(e) => console.log("row clicked", e.rowIndex)}
      ></AgGridReact>
    </div>
  );
};

export default ManageEmailGridComponent;
