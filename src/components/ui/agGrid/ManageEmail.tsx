import React from "react";
import ReactDOM from "react-dom";
import "./App.css";

import ManageEmailGridComponent from "./ManageEmailGridComponent";

const title = "Hello World";

function ManageEmail() {
  function createData(userFunction: string, email: string) {
    return { userFunction, email };
  }

  const dataGridDetailedRecords = [
    createData("UAT OPR", "marty.overdear@hexagonusfederal.com"),
    createData("UAT OPR", "ff.dd@ffds.com"),
    createData("FM (Functional Manager)", ""),
    createData("FM (Functional Manager)", "FM@fm.com"),
    createData("FM (Functional Manager)", "a2@fm.com"),
    createData("FM (Functional Manager)", "a2b@fm.com"),
    createData("FM (Functional Manager)", "b@fm.com"),
    createData("Test 0", "test0@abc.com"),
    createData("Test 1", "test1@abc.com"),
    createData("Test 2", "test2@abc.com"),
    createData("Test 3", "test3@abc.com"),
    createData("Test 4", "test4@abc.com"),
    createData("Test 5", "test5@abc.com"),
    createData("Test 6", "test6@abc.com"),
    createData("Test 7", "test7@abc.com"),
    createData("Test 8", "test8@abc.com"),
    createData("Test 9", "test9@abc.com"),
    createData("Test a", "testa@abc.com"),
    createData("Test b", "testb@abc.com"),
    createData("Test c", "testc@abc.com"),
    createData("Test d", "testd@abc.com"),
    createData("Test e", "teste@abc.com"),
    createData("Test f", "testf@abc.com"),
  ];

  return (
    <div className="App">
      <ManageEmailGridComponent
        dataGridDetailedRecords={dataGridDetailedRecords}
      />
    </div>
  );
}

ReactDOM.render(<ManageEmail />, document.getElementById("root"));

export default ManageEmail;
