import Enzyme, {mount, shallow} from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import ManageEmailGridComponent from "./ManageEmailGridComponent";
import React from "react";
// @ts-ignore
import {render, wait} from '@testing-library/react';
import {getByTestId, fireEvent} from '@testing-library/dom';

describe("<ManageEmailGridComponent />", () => {
    Enzyme.configure({adapter: new Adapter()});

    function createData(userFunction: string, email: string) {
        return {userFunction, email};
    }

    const dataGridDetailedRecords = [
        createData("UAT OPR", "marty.overdear@hexagonusfederal.com"),
        createData("UAT OPR", "ff.dd@ffds.com"),
        createData("FM (Functional Manager)", ""),
        createData("FM (Functional Manager)", "FM@fm.com"),
        createData("FM (Functional Manager)", "a2@fm.com"),
        createData("FM (Functional Manager)", "a2b@fm.com"),
        createData("FM (Functional Manager)", "b@fm.com"),
        createData("Test 0", "test0@abc.com"),
        createData("Test 1", "test1@abc.com"),
        createData("Test 2", "test2@abc.com"),
        createData("Test 3", "test3@abc.com"),
        createData("Test 4", "test4@abc.com"),
        createData("Test 5", "test5@abc.com"),
        createData("Test 6", "test6@abc.com"),
        createData("Test 7", "test7@abc.com"),
        createData("Test 8", "test8@abc.com"),
        createData("Test 9", "test9@abc.com"),
        createData("Test a", "testa@abc.com"),
        createData("Test b", "testb@abc.com"),
        createData("Test c", "testc@abc.com"),
        createData("Test d", "testd@abc.com"),
        createData("Test e", "teste@abc.com"),
        createData("Test f", "testf@abc.com"),
    ];

    test("ManageEmailGridComponent - test Heartbeat / Snapshot", () => {
        const component = mount(
            <ManageEmailGridComponent
                dataGridDetailedRecords={dataGridDetailedRecords}
            />
        );
        expect(component).toMatchSnapshot();
        component.unmount();
    });

    test("ManageEmailGridComponent - test data was returned", () => {
        const component = mount(
            <ManageEmailGridComponent dataGridDetailedRecords={dataGridDetailedRecords}/>,
            {}
        );
        // expect(component.find("ManageEmailGridComponent").prop("dataGridDetailedRecords")).toEqual(true);
        expect(
            component.find("ManageEmailGridComponent").prop("dataGridDetailedRecords")
        ).not.toBeNull();
        component.unmount();
    });

    test("ManageEmailGridComponent - mount get elements", () => {
        const component = mount(
            <ManageEmailGridComponent
                dataGridDetailedRecords={dataGridDetailedRecords}
            />,
            {}
        );

        console.log(component.debug());

        // the following get out of memory error and I don't know why
        // expect(component.find("div").getElements()).toHaveLength(1);
        // expect(component.find("Memo(HeaderRowComp)").getElements()).toHaveLength(1);
        // expect(component.find("Memo(HeaderCellComp)").getElements()).toHaveLength(1);

        expect(component.find("ManageEmailGridComponent").getElements()).toHaveLength(1);
        expect(component.find("AgGridReact").getElements()).toHaveLength(1);
        expect(component.find("AgGridReactUi").getElements()).toHaveLength(1);
        expect(component.find("Memo(GridComp)").getElements()).toHaveLength(1);
        expect(component.find("ForwardRef(TabGuardCompRef)").getElements()).toHaveLength(1);
        expect(component.find("Memo(GridBodyComp)").getElements()).toHaveLength(1);
        expect(component.find("Memo(GridHeaderComp)").getElements()).toHaveLength(1);
        expect(component.find("Memo(HeaderRowContainerComp)").getElements()).toHaveLength(3);
        expect(component.find("Memo(GridHeaderComp)").getElements()).toHaveLength(1);

        component.unmount();
    });

    test("ManageEmailGridComponent - shallow", () => {
        const component = shallow(
            <ManageEmailGridComponent
                dataGridDetailedRecords={dataGridDetailedRecords}
            />,
            {}
        );

        console.log(component.debug());

        expect(component.find("div").getElements()).toHaveLength(1);
        expect(component.find("#manageEmailTable")).toHaveLength(1);
        expect(component.find(".ag-theme-alpine").prop("rowSelection")).toEqual("multiple");

        component.unmount();
    });

    test("ManageEmailGridComponent - shallow - test buttons", () => {
        const component = shallow(
            <ManageEmailGridComponent
                dataGridDetailedRecords={dataGridDetailedRecords}
            />,
            {}
        );
        const rowSelectedAction = jest.fn();

        // expect(component.find(".ag-row-selected").prop("row-index")).toEqual("0");
        // expect(component.find(".ag-row-selected")).toHaveLength(1);
        //  component.find(".ag-row-selected").simulate("click");
        // expect(rowSelectedAction.mock.calls.length).toBe(1);
    });

    // you probably want to have some sort of timeout here so this doesn't hang in the event of some
    // other grid related issue
    const ensureGridApiHasBeenSet = (component: any) => {
        return new Promise<void>(function (resolve, reject) {
            (function waitForGridReady() {
                // eslint-disable-next-line testing-library/prefer-screen-queries
                if (getByTestId(component.container, "api").innerHTML !== "") {
                    return resolve();
                }
                setTimeout(waitForGridReady, 100);
            })();
        });
    };

    describe('row selection', () => {

        test('all rows selected', async () => {
            const component = shallow(
                <ManageEmailGridComponent
                    dataGridDetailedRecords={dataGridDetailedRecords}
                />,
                {}
            );
            // await wait(() => ensureGridApiHasBeenSet(component));
            // eslint-disable-next-line testing-library/prefer-screen-queries
            // @ts-ignore
            // eslint-disable-next-line testing-library/prefer-screen-queries
            // let selectAllButton = getByTestId(component.contains, 'selectAll');
            // fireEvent.click(selectAllButton);
            // // eslint-disable-next-line testing-library/no-node-access
            // let selectedNodes = component.contains.querySelectorAll('.ag-center-cols-container .ag-row-selected');
            // expect(selectedNodes.length).toBe(3);
        });
        //
        // test('all rows deselected', () => {
        //     let deSelectAllButton = getByTestId(appComponent.container, 'deSelectAll');
        //     fireEvent.click(deSelectAllButton);
        //     let selectedNodes = appComponent.container.querySelectorAll('.ag-center-cols-container .ag-row-selected');
        //     expect(selectedNodes.length).toBe(0);
        // });
    });
});