export type ShipDataGridDetailedRecord = {
  name: string;
  designation: string;
  number: string;
  shipClass: string;
};

export type ShipDataGridProperties = {
  shipDataGridProperties: ShipDataGridDetailedRecord[];
};
