export type DataGridDetailedRecord = {
    userId: string,
    ieUserAccess: string,
    fit: string,
    rsc: string,
    jost: string,
    spg: string,
    name: string
};

export type DataGridProperties = {
    dataGridDetailedRecords: DataGridDetailedRecord[];
};



