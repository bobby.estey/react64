import Enzyme, { mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import TableComponent from "./TableComponent";

describe("<TableComponent />", () => {
  Enzyme.configure({ adapter: new Adapter() });

  function createData(
    userId: string,
    ieUserAccess: string,
    fit: string,
    rsc: string,
    jost: string,
    spg: string,
    name: string
  ) {
    return { userId, ieUserAccess, fit, rsc, jost, spg, name };
  }

  const dataGridDetailedRecords = [
    createData("ABE", "NAN", "0", "1", "2", "3", "first last"),
    createData("ABF", "", "0", "3287263", "2", "3287263", "first last"),
    createData("ABH", "4", "0", "1", "2", "3", ""),
    createData("AC", "4", "2", "3", "1", "2", "Maricopa Arizona"),
    createData("AD", "0", "1", "2", "3", "0", "Mesa Arizona"),
    createData("AE", "2", "0", "0", "0", "0", "Phoenix Arizona"),
    createData("AG", "1", "1", "2", "3", "2", "Havasu Falls"),
    createData("AMH", "3", "1", "2", "2", "2", "Sedona Arizona"),
    createData("AMS", "1", "1", "2", "2", "1", "Ruidoso New Mexico USA"),
    createData("AO", "0", "2", "3", "0", "0", "Estey City New Mexico USA"),
    createData("AQ", "2", "0", "1", "0", "2", "Lincoln County New Mexico USA"),
    createData("AT", "2", "2", "1", "1", "0", "Omaha Nebraska"),
    createData("AW", "3", "2", "3", "0", "0", "Colorado Springs Colorado"),
    createData("AZ", "2", "2", "3", "0", "0", "Northglenn Colorado"),
    createData("PR", "0", "2", "3", "0", "0", "Hayes Virginia"),
  ];

  test("TableComponent - test Heartbeat / Snapshot", () => {
    const component = mount(
      <TableComponent
        dataGridDetailedRecords={dataGridDetailedRecords}
      />,
      {}
    );
    expect(component).toMatchSnapshot();
    component.unmount();
  });

  test("TableComponent - test data was returned", () => {
    const component = mount(
      <TableComponent
        dataGridDetailedRecords={dataGridDetailedRecords}
      />,
      {}
    );

    expect(component.find("TableComponent").prop("dataGridDetailedRecords")).not.toBeNull();
    component.unmount();
  });

  test("TableComponent - mount get elements", () => {
    const component = mount(
      <TableComponent
        dataGridDetailedRecords={dataGridDetailedRecords}
      />,
      {}
    );
    expect(component.find("div").getElements()).toHaveLength(162);
    expect(component.find("th").getElements()).toHaveLength(7);

    component.unmount();
  });

  test("TableComponent - shallow", () => {
    const component = shallow(
      <TableComponent
        dataGridDetailedRecords={dataGridDetailedRecords}
      />,
      {}
    );

    expect(component.find("#jostABE")).toBeTruthy();
    expect(component.find("div").getElements()).toHaveLength(100);
    expect(component.find("ForwardRef(Checkbox)").getElements()).toHaveLength(
      69
    );
    expect(component.find("#nameAD")).toHaveLength(1);

    expect(component.find("#userIdAD").prop("label")).toEqual("AD");
    expect(component.find("#ieUserAccessAD").prop("value")).toEqual("0");
    expect(component.find("#fitAD").prop("value")).toEqual("1");
    expect(component.find("#rscAD").prop("value")).toEqual("2");
    expect(component.find("#spgAD").prop("value")).toEqual("0");
    expect(component.find("#nameAD").prop("label")).toEqual("Mesa Arizona");

    console.log(component.debug());

    component.unmount();
  });
});
