import * as React from "react";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import { Button, Grid } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import ArrowBackIosNewIcon from "@mui/icons-material/ArrowBackIosNew";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import CreateIcon from "@mui/icons-material/Create";
import DeleteIcon from "@mui/icons-material/Delete";
import LockIcon from "@mui/icons-material/Lock";
import PrintIcon from "@mui/icons-material/Print";
import QuestionMark from "@mui/icons-material/QuestionMark";
import SaveIcon from "@mui/icons-material/Save";
import { TableComponentImageSX } from "./TableComponentSX";

const AdministrationComponent: React.FC = (props) => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={6}>
        <IconButton
          id="iconButton"
          type="submit"
          sx={TableComponentImageSX}
          aria-label="search"
        >
          <SearchIcon />
        </IconButton>
        <InputBase
          sx={{ ml: 1, flex: 1 }}
          placeholder="Search By USER ID"
          inputProps={{ "aria-label": "search by User Id" }}
        />
        <Button id="spgButton" variant="contained">
          SPG Privileges
        </Button>
        &nbsp;&nbsp;
        <Button id="jostButton" variant="contained">
          JOST Privileges
        </Button>
      </Grid>
      <Grid item xs={3}>
        <QuestionMark id="questionMark" sx={TableComponentImageSX} />
        &emsp;&emsp;&emsp;&emsp;
        <ArrowBackIosNewIcon
          id="arrowBackIosNewIcon"
          sx={TableComponentImageSX}
        />
        <AddIcon id="addIcon" sx={TableComponentImageSX} />
        <ArrowForwardIosIcon
          id="arrowForwardIosIcon"
          sx={TableComponentImageSX}
        />
      </Grid>
      <Grid item xs={3}>
        <SaveIcon id="saveIcon" sx={TableComponentImageSX} />
        <LockIcon id="lockIcon" sx={TableComponentImageSX} />
        <CreateIcon id="createIcon" sx={TableComponentImageSX} />
        <PrintIcon id="printIcon" sx={TableComponentImageSX} />
        <DeleteIcon id="deleteIcon" sx={TableComponentImageSX} />
      </Grid>
    </Grid>
  );
};

export default AdministrationComponent;
