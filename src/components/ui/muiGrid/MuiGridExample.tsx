import React from "react";
import "./App.css";

import MainApplicationContainer from "./ApplicationContainer";

const title = "Hello World";

function App() {
  function createData(
    userId: string,
    ieUserAccess: string,
    fit: string,
    rsc: string,
    jost: string,
    spg: string,
    name: string
  ) {
    return { userId, ieUserAccess, fit, rsc, jost, spg, name };
  }

  const dataGridDetailedRecords = [
    createData("ABE", "NAN", "0", "1", "2", "3", "first last"),
    createData("ABF", "", "0", "3287263", "2", "3287263", "first last"),
    createData("ABH", "4", "0", "1", "2", "3", ""),
    createData("AC", "4", "2", "3", "1", "2", "Maricopa Arizona"),
    createData("AD", "0", "1", "2", "3", "0", "Mesa Arizona"),
    createData("AE", "2", "0", "0", "0", "0", "Phoenix Arizona"),
    createData("AG", "1", "1", "2", "3", "2", "Havasu Falls"),
    createData("AMH", "3", "1", "2", "2", "2", "Sedona Arizona"),
    createData("AMS", "1", "1", "2", "2", "1", "Ruidoso New Mexico USA"),
    createData("AO", "0", "2", "3", "0", "0", "Estey City New Mexico USA"),
    createData("AQ", "2", "0", "1", "0", "2", "Lincoln County New Mexico USA"),
    createData("AT", "2", "2", "1", "1", "0", "Omaha Nebraska"),
    createData("AW", "3", "2", "3", "0", "0", "Colorado Springs Colorado"),
    createData("AZ", "2", "2", "3", "0", "0", "Northglenn Colorado"),
    createData("PR", "0", "2", "3", "0", "0", "Hayes Virginia"),
  ];

  return (
    <div className="App">
      <MainApplicationContainer
        dataGridDetailedRecords={dataGridDetailedRecords}
      />
    </div>
  );
}

export default App;
