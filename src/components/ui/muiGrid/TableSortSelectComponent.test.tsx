import Enzyme, { mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import TableSortSelectComponent from "./TableSortSelectComponent";
import React from "react";

describe("<TableSortSelectComponent />", () => {
  Enzyme.configure({ adapter: new Adapter() });

  function createData(
    name: string,
    designation: string,
    number: string,
    shipClass: string
  ) {
    return { name, designation, number, shipClass };
  }

  const dataGridDetailedRecords = [
    createData("Forrestal", "CV", "59", "Forrestal"),
    createData("Saratoga", "CV", "60", "Forrestal"),
    createData("Ranger", "CV", "61", "Forrestal"),
    createData("Independence", "CV", "62", "Forrestal"),
    createData("Kitty Hawk", "CV", "63", "Kitty Hawk"),
    createData("Constellation", "CV", "64", "Kitty Hawk"),
    createData("Enterprise", "CVN", "65", "Enterprise"),
    createData("America", "CV", "66", "Kitty Hawk"),
    createData("Kennedy", "CV", "67", "Kitty Hawk"),
    createData("Nimitz", "CVN", "68", "Nimitz"),
    createData("Eisenhower", "CVN", "69", "Nimitz"),
    createData("Vincent", "CVN", "70", "Nimitz"),
    createData("Theodore Roosevelt", "CVN", "71", "Nimitz"),
    createData("Lincoln", "CVN", "72", "Nimitz"),
    createData("Washington", "CVN", "73", "Nimitz"),
    createData("Stennis", "CVN", "74", "Nimitz"),
    createData("Truman", "CVN", "75", "Nimitz"),
    createData("Reagan", "CVN", "76", "Nimitz"),
    createData("George Bush", "CVN", "77", "Nimitz"),
    createData("Ford", "CVN", "78", "Ford"),
  ];

  test("TableSortSelectComponent - test Heartbeat / Snapshot", () => {
    const component = mount(
      <TableSortSelectComponent
        shipDataGridProperties={dataGridDetailedRecords}
      />,
      {}
    );
    expect(component).toMatchSnapshot();
    component.unmount();
  });

  test("TableSortSelectComponent - test data was returned", () => {
    const component = mount(
      <TableSortSelectComponent
        shipDataGridProperties={dataGridDetailedRecords}
      />,
      {}
    );
    // expect(component.find("TableSortSelectComponent").prop("dataGridDetailedRecords")).toEqual(true);
    expect(
      component.find("TableSortSelectComponent").prop("dataGridDetailedRecords")
    ).not.toBeNull();
    component.unmount();
  });

  test("TableSortSelectComponent - shallow", () => {
    const component = mount(
      <TableSortSelectComponent
        shipDataGridProperties={dataGridDetailedRecords}
      />,
      {}
    );
    expect(component.find("div").getElements()).toHaveLength(71);
    expect(component.find("th").getElements()).toHaveLength(10);
    // expect(component.find("TableSortSelectComponent").prop("shipDataGridProperties")).not.toBeNull();
    // console.log(component.debug());

    component.unmount();
  });
});
