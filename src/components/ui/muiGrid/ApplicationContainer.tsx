import * as React from "react";
import ReactDOM from "react-dom";

import { DataGridProperties } from "./DataGridTypes";
import AdministrationComponent from "./AdministrationComponent";
import TableComponent from "./TableComponent";

import { Stack } from "@mui/material";

const MainApplicationContainer: React.FC<DataGridProperties> = (props) => {
  const { dataGridDetailedRecords } = props;

  return (
    <div>
      <Stack direction="row" spacing={20}>
        <AdministrationComponent />
      </Stack>

      <div>
        <TableComponent
          dataGridDetailedRecords={dataGridDetailedRecords}
        />
      </div>
    </div>
  );
};

// ReactDOM.render(
//   <MainApplicationContainer />,
//   document.getElementById("root")
// );

export default MainApplicationContainer;
