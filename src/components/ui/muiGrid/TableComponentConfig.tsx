import { ReactElement, ReactNode } from "react";

// Props for the Table Component
export type TableComponentConfig = {
  userId: string;
  ieUserAccess: string;
  fit: string;
  rsc: string;
  jost: string;
  spg: string;
  name: string;
};
