import Enzyme, { mount, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import AdministrationComponent from "./AdministrationComponent";
import * as React from "react";

describe("<AdministrationComponent />", () => {
  Enzyme.configure({ adapter: new Adapter() });

  test("AdministrationComponent - test Heartbeat / Snapshot", () => {
    const component = mount(<AdministrationComponent />, {});
    expect(component).toMatchSnapshot();
    component.unmount();
  });

  test("AdministrationComponent - mount get elements", () => {
    const component = mount(<AdministrationComponent />, {});
    expect(component.find("div").getElements()).toHaveLength(5);
    expect(component.find("ForwardRef(InputBase)").getElements()).toHaveLength(
      1
    );
    expect(component.find("ForwardRef(InputBase)").prop("placeholder")).toEqual(
      "Search By USER ID"
    );
    expect(component.find("button").getElements()).toHaveLength(3);
    expect(
      component.find("ForwardRef(QuestionMarkIcon)").getElements()
    ).toHaveLength(1);
    expect(component.find("#questionMark")).toHaveLength(4);

    component.unmount();
  });

  test("AdministrationComponent - shallow - test buttons", () => {
    const iconButtonAction = jest.fn();
    const spgButtonAction = jest.fn();
    const jostButtonAction = jest.fn();
    const questionMarkAction = jest.fn();
    const arrowBackIosNewIconAction = jest.fn();
    const addIconAction = jest.fn();
    const arrowForwardIosIconAction = jest.fn();
    const saveIconAction = jest.fn();
    const lockIconAction = jest.fn();
    const createIconAction = jest.fn();
    const printIconAction = jest.fn();
    const deleteIconAction = jest.fn();

    const component = shallow(<AdministrationComponent />, {});

    expect(component.find("ForwardRef(Grid)")).toBeTruthy();

    expect(component.find("#iconButton")).toHaveLength(1);
    component.find("#iconButton").simulate("click");
    expect(iconButtonAction.mock.calls.length).toBe(0);

    expect(component.find("#spgButton")).toHaveLength(1);
    component.find("#spgButton").simulate("click");
    expect(spgButtonAction.mock.calls.length).toBe(0);

    expect(component.find("#jostButton")).toHaveLength(1);
    component.find("#jostButton").simulate("click");
    expect(jostButtonAction.mock.calls.length).toBe(0);

    expect(component.find("#questionMark")).toHaveLength(1);
    component.find("#questionMark").simulate("click");
    expect(questionMarkAction.mock.calls.length).toBe(0);

    expect(component.find("#arrowBackIosNewIcon")).toHaveLength(1);
    component.find("#arrowBackIosNewIcon").simulate("click");
    expect(arrowBackIosNewIconAction.mock.calls.length).toBe(0);

    expect(component.find("#addIcon")).toHaveLength(1);
    component.find("#addIcon").simulate("click");
    expect(addIconAction.mock.calls.length).toBe(0);

    expect(component.find("#arrowForwardIosIcon")).toHaveLength(1);
    component.find("#arrowForwardIosIcon").simulate("click");
    expect(arrowForwardIosIconAction.mock.calls.length).toBe(0);

    expect(component.find("#saveIcon")).toHaveLength(1);
    component.find("#saveIcon").simulate("click");
    expect(saveIconAction.mock.calls.length).toBe(0);

    expect(component.find("#lockIcon")).toHaveLength(1);
    component.find("#lockIcon").simulate("click");
    expect(lockIconAction.mock.calls.length).toBe(0);

    expect(component.find("#createIcon")).toHaveLength(1);
    component.find("#createIcon").simulate("click");
    expect(createIconAction.mock.calls.length).toBe(0);

    expect(component.find("#printIcon")).toHaveLength(1);
    component.find("#printIcon").simulate("click");
    expect(printIconAction.mock.calls.length).toBe(0);

    expect(component.find("#deleteIcon")).toHaveLength(1);
    component.find("#deleteIcon").simulate("click");
    expect(deleteIconAction.mock.calls.length).toBe(0);

    console.log(component.debug());

    component.unmount();
  });
});
