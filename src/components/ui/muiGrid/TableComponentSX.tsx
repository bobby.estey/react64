export const TableComponentPageSX = {
  width: "100%",
  overflow: "hidden",
};

export const TableComponentContainerSX = {
  maxHeight: 800,
};

export const TableComponentHeaderCellSX = {
  fontFamily: "Lato",
  fontStyle: "semi-bold",
  fontSize: "16px",
  ceilHeight: "48px",
  color: "#00529a",
  cellColor: "#ffffff",
  align: "center",
  width: 25,
};

export const TableComponentImageSX = {
  backgroundColor: "#ffffff",
  color: "#00529a",
  // fontSize: "32px",
  verticalAlign: "center",
  p: "10px",
}

export const TableComponentTextFieldSX = {
  width: 300
}
