import * as React from "react";
import PropTypes from "prop-types";
import Box from "@mui/material/Box";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TableSortLabel from "@mui/material/TableSortLabel";
import Paper from "@mui/material/Paper";
import Checkbox from "@mui/material/Checkbox";
import { visuallyHidden } from "@mui/utils";
import { DataGridProperties } from "./DataGridTypes";
import TextField from "@mui/material/TextField";

import {
  TableComponentContainerSX,
  TableComponentHeaderCellSX,
  TableComponentPageSX,
  TableComponentTextFieldSX,
} from "./TableComponentSX";

const TableComponent: React.FC<
  DataGridProperties
> = (props) => {
  const { dataGridDetailedRecords } = props;

  interface Column {
    id: string;
    label: string;
  }

  const columns: readonly Column[] = [
    {
      id: "userId",
      label: "USER ID",
    },
    {
      id: "ieUserAccess",
      label: "IE USER ACCESS",
    },
    {
      id: "fit",
      label: "FIT",
    },
    {
      id: "rsc",
      label: "RSC",
    },
    {
      id: "jost",
      label: "JOST",
    },
    {
      id: "spg",
      label: "SPG",
    },
    {
      id: "name",
      label: "NAME",
    },
  ];

  function descendingComparator(a: any, b: any, orderBy: any) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator(order: any, orderBy: any) {
    return order === "desc"
      ? (a: any, b: any) => descendingComparator(a, b, orderBy)
      : (a: any, b: any) => -descendingComparator(a, b, orderBy);
  }

  function stableSort(array: any, comparator: any) {
    const stabilizedThis = array.map((el: any, index: any) => [el, index]);
    stabilizedThis.sort((a: any, b: any) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) {
        return order;
      }
      return a[1] - b[1];
    });
    return stabilizedThis.map((el: any) => el[0]);
  }

  function EnhancedTableHead(props: any) {
    const { order, orderBy, onRequestSort } = props;
    const createSortHandler = (property: any) => (event: any) => {
      onRequestSort(event, property);
    };

    return (
      <TableHead>
        <TableRow>
          {columns.map((column) => (
            <TableCell
              sx={TableComponentHeaderCellSX}
              key={column.id}
              sortDirection={orderBy === column.id ? order : false}
            >
              <TableSortLabel
                active={orderBy === column.id}
                direction={orderBy === column.id ? order : "asc"}
                onClick={createSortHandler(column.id)}
              >
                {column.label}
                {orderBy === column.id ? (
                  <Box component="span" sx={visuallyHidden}>
                    {order === "desc"
                      ? "sorted descending"
                      : "sorted ascending"}
                  </Box>
                ) : null}
              </TableSortLabel>
            </TableCell>
          ))}
        </TableRow>
      </TableHead>
    );
  }

  EnhancedTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,

    order: PropTypes.oneOf(["asc", "desc"]).isRequired,
    orderBy: PropTypes.string.isRequired,
  };

  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("ieUserAccess");

  const handleRequestSort = (event: any, property: any) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const getWidget = (userId: string, columnId: string, value: string) => {
    if (columnId === "userId") {
      return (
        <div>
          <TextField
            sx={TableComponentTextFieldSX}
            id={columnId + userId}
            label={value}
            variant="outlined"
          />
        </div>
      );
    } else if (columnId === "name") {
      return (
        <div>
          <TextField
            sx={TableComponentTextFieldSX}
            disabled
            id={columnId + userId}
            label={value}
            variant="filled"
          />
        </div>
      );
    } else {
      switch (value) {
        case "0":
          return (
            <div>
              <Checkbox id={columnId + userId} value={value} disabled />
            </div>
          );
        case "1":
          return (
            <div>
              <Checkbox id={columnId + userId} value={value} disabled checked />
            </div>
          );
        case "2":
          return (
            <div>
              <Checkbox id={columnId + userId} value={value} />
            </div>
          );
        case "3":
          return (
            <div>
              <Checkbox id={columnId + userId} value={value} defaultChecked />
            </div>
          );
      }
    }
  };

  return (
    <div>
      <Paper sx={TableComponentPageSX}>
        <TableContainer sx={TableComponentContainerSX}>
          <Table stickyHeader aria-label="sticky table">
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
            />
            <TableBody>
              {stableSort(
                dataGridDetailedRecords,
                getComparator(order, orderBy)
              ).map((row: any) => {
                return (
                  <TableRow hover tabIndex={-1} key={row.userId}>
                    <TableCell>
                      {getWidget(row.userId, "userId", row.userId)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "ieUserAccess", row.ieUserAccess)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "fit", row.fit)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "rsc", row.rsc)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "jost", row.jost)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "spg", row.spg)}
                    </TableCell>
                    <TableCell>
                      {getWidget(row.userId, "name", row.name)}
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default TableComponent;
