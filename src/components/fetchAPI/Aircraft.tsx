export type Aircraft = {
	aircraftRegistrationNumber: string;
	aircraftSize: string;
	aircraftType: string;
};