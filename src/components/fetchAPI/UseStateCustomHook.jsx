// https://www.youtube.com/watch?v=ADe72qZ1_jc
// https://reqres.in/api/users
// https://jsonplaceholder.typicode.com/users
// http://localhost:6464/users/getAllUsers
// https://cvacv64.us/public/json/one.json
// http://api.tvmaze.com/search/shows?q=Vikings

import React, { useEffect, useState } from 'react';
import useFetch from "../hooks/useFetch"

function UseStateCustomHook() {

    const url = "https://jsonplaceholder.typicode.com/users";

    const [loading, setLoading] = useState(true)
    const [error, setError] = useState(null)
    const [data, setData] = useState([])

     // show in console
    // const response = useFetch(url, {})
    // console.log("console: " + response)

    // destructure
    // const {data, loading, error} = useFetch(url, {})

    // if(loading) return <h3>Loading...</h3>

    async function fetchData() {
        console.log('fetchData');
        
        const {data, loading, error} = useFetch(url, {})
    }

    return (
        <div>
            <button onClick={fetchData}>Call fetchData function</button>
            {data.map((field) => (
                // <h3 key={field.id}>{field.name} : {field.id} : {field.email} </h3>
                // <h3 key={field.score}>{field.score} </h3>
                <h3 key={field.id}>{field.name} : {field.username} </h3>
            ))}
        </div>
    )
}

export default UseStateCustomHook
