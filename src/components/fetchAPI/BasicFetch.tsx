// https://www.youtube.com/watch?v=aprOT-hp_Ho
// http://localhost:6464/users/getAllUsers
// https://cvacv64.us/public/devry/json/test.json
// http://api.tvmaze.com/search/shows?q=Vikings
// http://echo.jsontest.com/key/value/one/two

// 'Authorization': 'Basic Y3Y2NDpwYXNzd29yZA==',

import React, { useEffect, useState } from 'react';
import '../../App.css';

const BasicFetch: React.FC = (props) => {

    const urlNoHeaders = "http://api.tvmaze.com/search/shows?q=Vikings";
    const urlHeaders = "http://localhost:6464/users/getAllUsers";

    const [items, setItems] = useState([]);
    const [error, setError] = useState(null);

    async function fetchDataNoHeaders() {
        console.log('fetchDataNoHeaders');

        await fetch(urlNoHeaders)
            .then((result) => result.json())
            .then((data: any) => {
                console.log('fetchDataNoHeaders success');
                console.log(data);
                setItems(data);
                return data;
            })
            .catch(() => {
                console.log('fetchDataNoHeaders catch');
                console.log(error);
                setError(error);
            })
            .finally(() => {
                console.log('fetchDataNoHeaders finally');
            });
    }

    async function fetchDataHeaders() {
        console.log('fetchDataHeaders');

        await fetch(urlHeaders, {
            method: 'GET',
            headers: {
                'Authorization': 'Basic Y3Y2NDpwYXNzd29yZA==',
                'Content-Type': 'application/json'
            }
        })
            .then((result) => result.json())
            .then((data: any) => {
                console.log('fetchDataHeaders success');
                console.log(data);
                setItems(data);
                return data;
            })
            .catch(() => {
                console.log('fetchDataHeaders catch');
                setError(error);
            })
            .finally(() => {
                console.log('fetchDataHeaders finally');
            });
    }

    return (
        <div>
            <button onClick={fetchDataNoHeaders}>Call fetchDataNoHeaders</button>
            <br></br><br></br>
            <button onClick={fetchDataHeaders}>Call fetchDataHeaders</button>

            {/* {items && items.map(item => (
                <div key={item['show']['id']}>
                    <h3>show.id: {item['show']['id']} score: {item['score']} image: {item['show']['image']['original']}</h3>
                </div>
            ))} */}

            {items && items.map(item => (
                <div key={item['id']}>
                    <h3>id: {item['id']} name: {item['name']} email: {item['email']}</h3>
                </div>
            ))}

        </div>
    );
};

export default BasicFetch;
