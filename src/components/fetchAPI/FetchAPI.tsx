import React, { useEffect, useState } from "react";
import '../../App.css';
import { Aircraft } from "./Aircraft";

const FetchSpring: React.FC = (props) => {

	//	const {
	//		aircraftRegistrationNumber,
	//		aircraftSize,
	//		aircraftType
	//	} = props;

	const [rowData, setRowData] = useState([]);

	useEffect(() => {
		console.log("use effect");

		fetch(
			"http://localhost:6464/aircraftQueueManagerService/queueStatus", {
			method: "POST",
			headers: new Headers({
				"Content-Type": "application-json",
				"Access-Control-Allow-Origin": "*",
			})
		})
			.then((result) => result.json())
			.then((data: any) => {
				console.log("fetch useEffect data success");
				return data;
			})
			.catch(() => {
				console.log("fetch useEffect catch");
			});
	}, []);

	async function getQueueStatus() {
		console.log("getQueueStatus");

		fetch(
			"http://localhost:6464/aircraftQueueManagerService/queueStatus", {
			method: "POST",
			headers: new Headers({
				"Content-Type": "application-json",
				"Access-Control-Allow-Origin": "*",
			})
		})
			.then((result) => result.json())
			.then((rowData) =>
				setRowData(
					rowData.map((item: Aircraft) => ({
						aircraftRegistrationNumber: item.aircraftRegistrationNumber,
						aircraftSize: item.aircraftSize,
						aircraftType: item.aircraftType,
					})
					)))
			.catch(() => {
				console.log("fetch catch");
			});

	}

	return (
		<div>
			<h1> Fetch data from an api in react </h1>  {
				//	rowData.map((item: Aircraft) => ({
				//		aircraftRegistrationNumber: { item.aircraftRegistrationNumber },
				//		aircraftSize: { item.aircraftSize },
				//		aircraftType: { item.aircraftType }
				//	}))
			}
		</div>
	);
};

export default FetchSpring;
