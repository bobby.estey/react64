// https://www.youtube.com/watch?v=aprOT-hp_Ho
// http://localhost:6464/users/getAllUsers
// https://cvacv64.us/public/devry/json/test.json
// https://dev-api.camvio.cloud/aboss-api/rest/v1/account/49490/balance
// http://api.tvmaze.com/search/shows?q=Vikings
// http://echo.jsontest.com/key/value/one/two

// 'Authorization': 'Basic Y3Y2NDpwYXNzd29yZA==',

import React, { useEffect, useState } from 'react';
import '../../App.css';

const FetchSpring: React.FC = (props) => {

    const url = "http://api.tvmaze.com/search/shows?q=Vikings";

    const [items, setItems] = useState([]);
    const [error, setError] = useState(null);

    async function fetchDataNoHeaders() {
        console.log('fetchDataNoHeaders');

        await fetch(url)
            .then((result) => result.json())
            .then((data: any) => {
                console.log('fetchDataNoHeaders success');
                console.log(data);
                setItems(data);
                return data;
            })
            .catch(() => {
                console.log('fetchDataNoHeaders catch');
                console.log(error);
                setError(error);
            })
            .finally(() => {
                console.log('fetchDataNoHeaders finally');
            });
    }

    async function fetchDataHeaders() {
        console.log('fetchDataHeaders');

        await fetch('https://dev-api.camvio.cloud/aboss-api/rest/v1/account/49490/balance', {
            method: 'GET',
            headers: {
                'Authorization': 'Basic amF2YS5wb3NpdGlvbjphYmNBQkMxMjMh',
                'Content-Type': 'application/json',
                'X-API-Token': 'Hzsww7X0KY_hL7Rd_u7p1I-KLgymPg7EN6XpAmyz',
                'Cookie': 'JSESSIONID=DAAE09BC642CDB6A15A31ED6FE3B37B3'
            }
        })
            .then((result) => result.json())
            .then((data: any) => {
                console.log('fetchDataHeaders success');
                console.log(data);
                setItems(data);
                return data;
            })
            .catch(() => {
                console.log('fetchDataHeaders catch');
                setError(error);
            })
            .finally(() => {
                console.log('fetchDataHeaders finally');
            });
    }

    return (
        <div>
            <button onClick={fetchDataNoHeaders}>Call fetchDataNoHeaders</button>

            {items && items.map(item => (
                <div key={item['score']}>
                    <h3>score: {item['score']} showId: {item['show']['id']} image: {item['show']['image']['original']}</h3>
                </div>
            ))}

        </div>
    );
};

export default FetchSpring;
