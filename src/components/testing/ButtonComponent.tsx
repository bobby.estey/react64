import logo from '../../../src/logo.svg';
import '../../../src/App.css';

function ButtonComponent() {
	return (
		<div className="App">
			<header className="app-header">
				<img src={logo} className="App-logo" alt="logo" />
			</header>

			<div id="secondDiv">
				<p className="first-p">
					First Paragraph
				</p>
				<p id="second-p">
					Second Paragraph
				</p>

				React Testing with Jest and Enzyme

				<br /><br />

				<button id="1">one</button>&nbsp;&nbsp;
				<button id="2">two</button>
			</div>

			<br /><br />

			<footer>
				<b>Footer Section</b>
			</footer>
		</div>
	);
}

export default ButtonComponent;
