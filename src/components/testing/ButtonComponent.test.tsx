import Enzyme, { mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import Moment from "moment";
import ButtonComponent from "./ButtonComponent";

describe("<ButtonComponent />", () => {
  Enzyme.configure({ adapter: new Adapter() });

  let okCallback: any;
  let cancelCallback;

  let component: any;
  // let shallowComponent;
  let timestamp;

  const dialogConfig = {
    isDialogOpen: false,
    setIsDialogOpen: undefined,
    title: "title attribute",
    description: "description attribute",
    buttons: ["OK", "CANCEL"],
    buttonHref: [],
    callbacks: [],
  };

  const setUp = (props = {}) => {
    okCallback = jest.fn();
    cancelCallback = jest.fn();

    // const component = mount(<ButtonComponent {...dialogConfig} callbacks={[okCallback, cancelCallback]}/>);
    // @ts-ignore
    const component = mount(<ButtonComponent {...dialogConfig} />);

    //expect(okCallback).toHaveBeenCalled();
    // const shallowComponent = shallow(<ButtonComponent {...dialogConfig} />);

    return component;
  };

  const setLogging = (props = {}) => {
    console.log(
      "<<<<<ButtonComponent - component.debug>>>>>\n\n" +
        component.debug() +
        "\n\n<<<<<ButtonComponent - component.debug>>>>>"
    );
  };

  beforeAll(() => {});

  beforeEach(() => {
    timestamp = Date.now();
    console.log(
      "ButtonComponent.beforeEach: " +
        Moment(new Date(), Moment.ISO_8601).toString()
    );
    // console.log(new Intl.DateTimeFormat('default', {year: 'numeric', month: '2-digit',day: '2-digit', hour: '2-digit', minute: '2-digit', second: '2-digit'}).format(timestamp));

    component = setUp();
    setLogging();
  });

  afterEach(() => {
    // expect(component).toMatchSnapshot();
    component.unmount();
    console.log(
      "ButtonComponent.afterEach: " +
        Moment(new Date(), Moment.ISO_8601).toString()
    );
  });

  afterAll(() => {});

  test("screen.getByText", () => {
    // const linkElement = screen.getByText(/React Testing/i);
    // expect(linkElement).toBeInTheDocument();
  });

  test("ButtonComponentElementsStructure", () => {
    expect(component.find("div").getElements()).toHaveLength(2);
    expect(component.find("header").getElements()).toHaveLength(1);
    expect(component.find("p").getElements()).toHaveLength(2);
    expect(component.find("button").getElements()).toHaveLength(2);
    expect(component.find("br").getElements()).toHaveLength(4);
    expect(component.find("footer").getElements()).toHaveLength(1);

    const buttonTwo = component.find('button[children="two"]');
    expect(buttonTwo).toHaveLength(1);
    //buttonTwo.simulate('click');

    // expect(okCallback).toHaveBeenCalled();
    // const clickMock = jest.fn();
    //
    // const btn = component.find("#loginBtn");
    // if (btn) btn.simulate("click");
    // expect(clickMock.mock.calls.length).toBe(1);
  });

  test("AppNodesStructure", () => {
    // expect(component.find("TddTutorial").childAt(0).type()).toEqual(
    //   "div"
    // );
    // component.find("TddTutorial").forEach((child) => {
    //   console.log("AppNodesStructure.forEach: " + "\n\n");
    // expect(child.type).toBeTruthy();
    // });
  });

  test("AppRender", () => {
    // const component2 = render(component);
    // expect(component.title("abc")).toEqual("abc");
    // title: "title attribute",
    // description: "description attribute",
  });

  test("ButtonComponentAttributesStructure", () => {
    // expect(component.find("ButtonComponent").prop("isDialogOpen")).toEqual(
    //     false
    // );
  });

  test("ButtonComponentNodesStructure", () => {
    // expect(component.find("ButtonComponent").childAt(0).type()).toEqual(
    //     "div"
    // );
    // component.find("ButtonComponent").forEach((child) => {
    //   console.log("ButtonComponentNodesStructure.forEach: " + "\n\n");
    // expect(child.type).toBeTruthy();
    // });
  });

  test("ButtonComponentHandleClose", () => {
    // const setIsDialogOpen: boolean = ButtonComponent.handleClose();
    // expect(setIsDialogOpen).toStrictEqual(false);
    // expect(ButtonComponent.handleClose).toHaveBeenCalled();
  });

  test("ButtonComponentRender", () => {
    // const component2 = render(component);
    // expect(component.title("abc")).toEqual("abc");
    // expect(component.setIsDialogOpen(true)).toEqual(true);
    // isDialogOpen: false,
    // setIsDialogOpen: undefined,
    // title: "title attribute",
    // description: "description attribute",
    // buttons: ["OK", "CANCEL"],
    // buttonHref: [],
    // callbacks: [],
  });
});
