import Enzyme, { mount } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { render, screen } from '@testing-library/react';
import Parsing from './Parsing';

describe('<ParsinglicationContainer />', () => {
	Enzyme.configure({ adapter: new Adapter() });

	let component: any;
	// let shallowComponent;

	const config = {
		title: "title attribute",
		description: "description attribute"
	};

	const setUp = (props = {}) => {
		// @ts-ignore
		component = mount(<Parsing {...config}></Parsing>);
		// const shallowComponent = shallow(<Parsing {...config} />);
		return component;
	};

	beforeAll(() => {
		// console.log(
		//   "<<<<<ParsingTest - component.debug>>>>>\n\n" +
		//     component.debug() +
		//     "\n\n<<<<<ParsingTest - component.debug>>>>>"
		// );
	});

	beforeEach(() => {
		component = setUp();
	});

	afterEach(() => {
		// expect(component).toMatchSnapshot();
		component.unmount();
	});

	afterAll(() => {
	});

	test('screen.getByText', () => {
		render(<Parsing />);
		const linkElement = screen.getByText(/react testing/i);
		expect(linkElement).toBeInTheDocument();
	});

	test("ParsingElementsStructure", () => {
		expect(component.find('div').getElements()).toHaveLength(2);
		expect(component.find('header').getElements()).toHaveLength(1);
		expect(component.find('p').getElements()).toHaveLength(2);
		expect(component.find('button').getElements()).toHaveLength(2);

		const buttonTwo = component.find('button[children="two"]');
		expect(buttonTwo).toHaveLength(1);
		buttonTwo.simulate('click');


		console.log(component.find('#button2').getDOMNode().attributes.getNamedItem('value'));
	});

	test("ParsingPropertiesStructure", () => {
		expect(config).toHaveProperty("title");
		expect(config).toHaveProperty("description");
	});

	test("ParsingAttributesStructure", () => {
		expect(component.find("Parsing").prop("title")).toEqual(
			"title attribute"
		);
		expect(component.find("Parsing").prop("description")).toEqual(
			"description attribute"
		);
		expect(component.find("#outlined-basic").prop('label')).toEqual("Outlined");
		expect(component.find("#outlined-basic").prop('variant')).toEqual("outlined");
	});

	test("ParsingNodesStructure", () => {
		// expect(component.find("Parsing").childAt(0).type()).toEqual(
		//   "div"
		// );

		// component.find("Parsing").forEach((child) => {
		//   console.log("ParsingNodesStructure.forEach: " + "\n\n");
		// expect(child.type).toBeTruthy();
		// });
	});

	test("ParsingRender", () => {
		// const component2 = render(component);
		// expect(component.title("abc")).toEqual("abc");

		// title: "title attribute",
		// description: "description attribute",
	});
});
