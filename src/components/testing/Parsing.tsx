import logo from "../../logo.svg";
import "./Parsing.css";
import TextField from "@mui/material/TextField";

function Parsing() {
	return (
		<div className="parsing">
			<header className="parsing-header">
				<img src={logo} className="parsing-logo" alt="logo" />
			</header>

			<div id="secondDiv">
				<p className="first-p">
					Rending <code>src/Parsing.js</code>
				</p>
				<p id="second-p">Second Paragraph</p>
				React Testing with Jest and Enzyme
				<br />
				<br />
				<button id="button1" value="button1value">
					one
				</button>
				&nbsp;&nbsp;
				<button id="button2" value="button2value">
					two
				</button>
			</div>

			<br />
			<TextField id="outlined-basic" label="Outlined" variant="outlined" />
			<TextField id="filled-basic" label="Filled" variant="filled" />
			<TextField id="standard-basic" label="Standard" variant="standard" />
			<br />

			<footer>
				<b>Footer Section</b>
			</footer>
		</div>
	);
}

export default Parsing;
