import React from "react";
import '../../components/series/SeriesComponent.css';
import '../../components/series/SeriesComponent';
import './SeriesList.css';

// @ts-ignore
const SeriesListItem = ({series}) => (
    <li>
        {series.show.name}
    </li>
)

// @ts-ignore
const SeriesList = (props) => {
    return (
        <div>
            <ul className="series-list">
                {props.list.map((series: { show: { id: React.Key | null | undefined; }; }) => (
                    <SeriesListItem series={series} key={series.show.id}/>
                ))}
            </ul>
        </div>
    )
}

export default SeriesList;
