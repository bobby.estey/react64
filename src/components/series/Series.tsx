import React, { Component } from "react";
import SeriesList from "./SeriesList";

class Series extends Component<any, any> {
  state = {
    series: [],
    seriesName: "",
    isFetching: false,
  };

  // lifecycle hook - React instance is create, calls the componentDidMount function
  componentDidMount() {
    // fetch is going to this site and returning a Promise
    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then((response) => { console.log(response) })

    // fetch('http://api.tvmaze.com/search/shows?q=Vikings')
    // .then(response => response.json())
    // .then(json => console.log(json))

    fetch("http://api.tvmaze.com/search/shows?q=Vikings")
      .then((response) => response.json())
      .then((json) => this.setState({ series: json }));
  }

  render() {
    return (
      <div>
        The length of series array - {this.state.series.length}
        <SeriesList list={this.state.series} />
      </div>
    );
  }
}

export default Series;
