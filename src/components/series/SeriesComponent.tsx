import React, { Component } from "react";
import Intro from "../../containers/intro/Intro";
import "./SeriesComponent.css";
import Series from "./Series";

class SeriesComponent extends Component {
  // added Conditional Rendering
  render() {
    // const { series, seriesName, isFetching } = this.state;

    return (
      <div>
        <header className={"series-header"}>
          <h1 className={"series-header"}>TV Series List</h1>
        </header>
        <Intro message={"Here you can find all of your most loved series"} />

        <Series></Series>
      </div>
    );
  }
}

export default SeriesComponent;
