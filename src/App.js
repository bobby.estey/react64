import React from 'react';
import CustomHooksExample2 from "./components/hooks/CustomHooksExample2";

function App() {

  return (
    <div>
      <CustomHooksExample2 />
    </div>
  )
}

export default App
