import React from "react";
import { render, screen } from "@testing-library/react";
import {Heading} from "./Heading";

describe("<Heading />", () => {

  test("Heading - render heading", () => {
    const { getByText } = render(<Heading />);
    const linkElement = screen.getByText(/Hello React/i);
    expect(linkElement).toBeInTheDocument();
  });

  test("Heading - render heading with argument", () => {
    const { getByText } = render(<Heading name={`React`} />);
    const linkElement = screen.getByText(/Hello React/i);
    expect(linkElement).toBeInTheDocument();
  });
});
