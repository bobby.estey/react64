// https://youtu.be/JXrZDUzkc2Q?t=473
// Types for React Props and State - more constrained
// Interfaces have more capabilities, public API
import React from "react";

export type HeadingProps = { name?: string };

export function Heading({name = 'React'}: HeadingProps) {
    return <h1>Hello {name}</h1>;
}