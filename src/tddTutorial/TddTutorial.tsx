import React from "react";
import { Heading } from "./Heading";
import Counter from "./Counter";

export const label = (name: string) => {
  return `Hello ${name.toUpperCase()}`;
};

function TddTutorial() {
  return (
    <div>
      {/*<h1>{label("React")}</h1>*/}
      <Heading />
      <Counter label={"Current"} />
    </div>
  );
}

export default TddTutorial;
