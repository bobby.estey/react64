import React from "react";
import {fireEvent, render, screen} from "@testing-library/react";
import Counter from "./Counter";
import {mount} from "enzyme";
import ButtonComponent from "../components/testing/ButtonComponent";
import userEvent from "@testing-library/user-event";

describe("<Counter />", () => {
    test("render a label and counter", () => {
        // const { getByLabelText, getByRole } = mount(<Counter />);
        const {getByLabelText, getByRole} = render(<Counter/>);
        // const label = screen.getByLabelText("Count");
        // expect(label).toBeInTheDocument();
        const counter = screen.getByRole("counter");
        expect(counter).toBeInTheDocument();
    });

    test("should start at zero", () => {
        // const { getByLabelText, getByRole } = mount(<Counter />);
        const {getByRole} = render(<Counter/>);
        const counter = screen.getByRole("counter");
        expect(counter).toHaveTextContent("0");
    });

    test("should start at another value", () => {
        // const { getByLabelText, getByRole } = mount(<Counter />);
        const {getByRole} = render(<Counter start={10}/>);
        const counter = screen.getByRole("counter");
        expect(counter).toHaveTextContent("0");
    });

    test("should increment counter by ten", () => {
        // const { getByLabelText, getByRole } = mount(<Counter />);
        const {getByRole} = render(<Counter/>);
        const counter = screen.getByRole("counter");
        expect(counter).toHaveTextContent("0");
        // fireEvent.click(counter);
        userEvent.click(counter, {shiftKey: true});
        expect(counter).toHaveTextContent("10");
    });
});
