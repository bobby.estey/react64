import React from "react";
import TddTutorial, { label } from "./TddTutorial";
import { getByLabelText, render, screen } from "@testing-library/react";
import exp from "constants";

describe("<TddTutorial />", () => {
  test("TddTutorial - actual vs expected", () => {
    const { getByText, getByRole } = render(<TddTutorial />);
    const linkElement = screen.getByText(/Hello React/i);
    expect(linkElement).toBeInTheDocument();
    // const label = screen.getByLabelText("Current");
    // expect(label).toBeInTheDocument();
    const counter = screen.getByRole("counter");
    expect(counter).toHaveTextContent("0");

    // const actual = 1;
    // const expected = 1;
    // expect(actual).toEqual(expected);
  });

  // test("TddTutorial - generates a label", () => {
  //   const result = label("React");
  //   expect(result).toEqual("Hello REACT");
  // });
});
