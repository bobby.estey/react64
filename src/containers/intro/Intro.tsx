import React from "react";

// functional component - JavaScript function returning a React Component
// JSX - convention is Custom React Components start with Uppercase
// built in components start with lowercase

// props object contains attributes that can be utilized
// @ts-ignore
const Intro = (props) => (
    <p>{props.message}</p>
);

export default Intro;