# react64 - React Demonstration Site

- The project contains basic demonstrations and examples of React applications.  The key class to look at is [App.tsx](./src/App.tsx)
- The decision was to separate the applications inside a single umbrella architecture keeping node_modules as a single source
- All the applications that are executable are imported into the code
- To execute each individually, replace the Element in the return with the Component Element
     - the example below shows <DialogExample /> as the Component being executed

## Code Example

```
import React from "react";
import "./App.css";
imports of various examples...

const title = "react64 - React Demonstration Site";

function App() {
	return (
		<div className="App">
			{title}
        	<br></br>
			<br></br>
			<DialogExample />  **(change element to match the import, e.g. DialogExample)**
		</div>
	);
}

export default App;
```
